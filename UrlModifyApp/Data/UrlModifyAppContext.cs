﻿using Microsoft.EntityFrameworkCore;
using System;

namespace UrlModifyApp.Data
{
    public class UrlModifyAppContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //var connString = Environment.GetEnvironmentVariable("UrlModifyApp_CONNECTION");
            var connString = "Server=localhost;Port=6789;UserId=ANDPLUS;Password=andplus99;Database=ANDPLUS;"; //Environment.GetEnvironmentVariable("DbConnectionString");
            optionsBuilder.UseNpgsql(connString)
                .EnableSensitiveDataLogging();
        }

        public DbSet<UrlModifyApp.Models.UrlinfoModel> UrlinfoModel { get; set; }
        public DbSet<UrlModifyApp.Models.User> User { get; set; }
    }
}