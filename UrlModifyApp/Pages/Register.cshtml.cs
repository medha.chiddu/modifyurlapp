using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UrlModifyApp.Models;
using UrlModifyApp.Services;

namespace UrlModifyApp.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly UrlModifyApp.Data.UrlModifyAppContext _context;
        private IPasswordHashers _passwordHasher;

        public RegisterModel(UrlModifyApp.Data.UrlModifyAppContext context , IPasswordHashers passwordHasher)
        {
            _context = context;
            _passwordHasher = passwordHasher;
        }
        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public User user { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var email = _context.User.FirstOrDefault(i => i.EmailAddress == user.EmailAddress);

            if(email == null)
            {
                user.Password = _passwordHasher.HashPassword(user.Password);
                _context.User.Add(user);

                await _context.SaveChangesAsync();

                return RedirectToPage("./Index");
            }

            ModelState.AddModelError("error_msg", "Email address already exists");
            return Page();
        }
    }
}