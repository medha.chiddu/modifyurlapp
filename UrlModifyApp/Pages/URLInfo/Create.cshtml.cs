﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;
using UrlModifyApp.Models;
using UrlModifyApp.Services;

namespace UrlModifyApp.Pages.URLInfo
{
    public class CreateModel : PageModel
    {
        private readonly UrlModifyApp.Data.UrlModifyAppContext _context;
        private IShortenUrlService _urlShortenerService;

        [BindProperty]
        public UrlinfoModel UrlinfoModel { get; set; }

        public User user { get; set; }


        public CreateModel(UrlModifyApp.Data.UrlModifyAppContext context, IShortenUrlService urlShortenerService)
        {
            _context = context;
            _urlShortenerService = urlShortenerService;
        }

        public IActionResult OnGet(string id)
        {
            if (id.Equals("00000000-0000-0000-0000-000000000000") || Request.Cookies["MyCookie"] == null)
            {
                return NotFound();
            }
            return Page();
        }

        
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var shortUrl = await _urlShortenerService.ShortenUrlAsync(UrlinfoModel.LongUrl);
            UrlinfoModel.ShortUrl = shortUrl;

            UrlinfoModel.UserId = Guid.Parse(Request.Cookies["userId"]);
            _context.UrlinfoModel.Add(UrlinfoModel);
            await _context.SaveChangesAsync();
            return RedirectToPage("./Index");
        }
    }
}