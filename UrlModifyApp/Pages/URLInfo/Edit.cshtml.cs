﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using UrlModifyApp.Models;
using UrlModifyApp.Services;

namespace UrlModifyApp.Pages.URLInfo
{
    public class EditModel : PageModel
    {
        private readonly UrlModifyApp.Data.UrlModifyAppContext _context;
        private IShortenUrlService _urlShortenerService;

        public EditModel(UrlModifyApp.Data.UrlModifyAppContext context, IShortenUrlService urlShortenerService)
        {
            _context = context;
            _urlShortenerService = urlShortenerService;
        }

        [BindProperty]
        public UrlinfoModel UrlinfoModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id, Guid userId)
        {
            if (id == null || userId == null)
            {
                return NotFound();
            }
            UrlinfoModel = await _context.UrlinfoModel.FirstOrDefaultAsync(m => m.Id == id);

            if (UrlinfoModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var shortUrl = await _urlShortenerService.ShortenUrlAsync(UrlinfoModel.LongUrl);
            UrlinfoModel.ShortUrl = shortUrl;
            UrlinfoModel.UserId = Guid.Parse(Request.Cookies["userId"]);
            _context.Attach(UrlinfoModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UrlinfoModelExists(UrlinfoModel.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToPage("./Index");
        }

        private bool UrlinfoModelExists(int id)
        {
            return _context.UrlinfoModel.Any(e => e.Id == id);
        }
    }
}