﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using UrlModifyApp.Models;

namespace UrlModifyApp.Pages.URLInfo
{
    public class IndexModel : PageModel
    {
        private readonly UrlModifyApp.Data.UrlModifyAppContext _context;

        public IndexModel(UrlModifyApp.Data.UrlModifyAppContext context)
        {
            _context = context;
        }

        public IList<UrlinfoModel> UrlinfoModel { get; set; }

        public void OnGet()
        {
            var cookieValue = Request.Cookies["MyCookie"];
            if (cookieValue != null)
            {
                var id = Guid.Parse(Request.Cookies["UserId"]);
                ViewData["userId"] = id;
                UrlinfoModel = _context.UrlinfoModel.Where(i => i.UserId == id).ToList();
            }
            else
            {
                ModelState.AddModelError("error_msg", "Forbidden Access");
            }
        }
    }
}