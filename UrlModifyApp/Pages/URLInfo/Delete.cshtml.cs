﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using UrlModifyApp.Models;

namespace UrlModifyApp.Pages.URLInfo
{
    public class DeleteModel : PageModel
    {
        private readonly UrlModifyApp.Data.UrlModifyAppContext _context;
        

        public DeleteModel(UrlModifyApp.Data.UrlModifyAppContext context)
        {
            _context = context;
        }

        [BindProperty]
        public UrlinfoModel UrlinfoModel { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || Request.Cookies["MyCookie"] == null)
            {
                return NotFound();
            }

            UrlinfoModel = await _context.UrlinfoModel.FirstOrDefaultAsync(m => m.Id == id);

            if (UrlinfoModel == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || Request.Cookies["MyCookie"] == null)
            {
                return NotFound();
            }

            UrlinfoModel = await _context.UrlinfoModel.FindAsync(id);

            if (UrlinfoModel != null)
            {
                _context.UrlinfoModel.Remove(UrlinfoModel);
                await _context.SaveChangesAsync();
            }
            return RedirectToPage("./Index");
        }
    }
}