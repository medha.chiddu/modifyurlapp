﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UrlModifyApp.Models;
using UrlModifyApp.Services;

namespace UrlModifyApp.Pages
{
    public class GuestModel : PageModel
    {
        private IShortenUrlService _urlShortenerService;
        

        public GuestModel(IShortenUrlService urlShortenerService)
        {
            _urlShortenerService = urlShortenerService;
        }

        public void OnGet()
        {
            
        }
        
        public async Task<IActionResult> OnPostAsync(string LongUrl)
        {
            if(LongUrl.Length == 0 || LongUrl == null)
            {
                ViewData["ShortURL"] = "Please enter long url";
                return Page();
            }
            var shortUrl = await _urlShortenerService.ShortenUrlAsync(LongUrl);
            ViewData["ShortURL"] = shortUrl;
            return Page();
        }

    }
}