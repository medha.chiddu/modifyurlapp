﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using UrlModifyApp.Models;
using UrlModifyApp.Services;

namespace UrlModifyApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly UrlModifyApp.Data.UrlModifyAppContext _context;
        private IPasswordHashers _passwordHasher;
        
        public IndexModel(UrlModifyApp.Data.UrlModifyAppContext context , IPasswordHashers passwordHasher)
        {
            _context = context;
            _passwordHasher = passwordHasher;
        }

        public IActionResult OnGet()
        {
            Response.Cookies.Delete("MyCookie");
            Response.Cookies.Delete("userId");
            Response.Cookies.Delete("UserName");
            return Page();
        }

        public User user { get; set; }
       

        public IActionResult OnPost(string LoginFormEmail, string LoginFormPassword)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            try
            {
                var password = _context.User.FirstOrDefault(i => i.EmailAddress == LoginFormEmail).Password;

                if (_passwordHasher.VerifyHashedPassword(password,LoginFormPassword))
                {
                    var UserName = _context.User.FirstOrDefault(i => i.EmailAddress == LoginFormEmail).FirstName;
                    var id = _context.User.FirstOrDefault(i => i.EmailAddress == LoginFormEmail).Id.ToString();
                    CookieOptions option = new CookieOptions();
                    option.Expires = DateTime.Now.AddMinutes(60);
                    Response.Cookies.Append("MyCookie", "signedIn", option);
                    Response.Cookies.Append("UserId", id, option);
                    Response.Cookies.Append("UserName", UserName, option);
                    return RedirectToPage("./URLInfo/Index");
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("error_msg", "Email address and password does not match");
            }
            

            ModelState.AddModelError("error_msg", "Email address and password does not match");

            return Page();
        }
    }
}
