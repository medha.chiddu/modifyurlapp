﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace UrlModifyApp.Pages
{
    public class LogoutModel : PageModel
    {
        public IActionResult OnGet()
        {
            Response.Cookies.Delete("MyCookie");
            Response.Cookies.Delete("userId");
            Response.Cookies.Delete("UserName");
            return RedirectToPage("./Index");
        }
    }
}