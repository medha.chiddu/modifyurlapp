﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UrlModifyApp.Migrations
{
    public partial class AddFKtoURl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UrlinfoModel_User_UserId",
                table: "UrlinfoModel");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "UrlinfoModel",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UrlinfoModel_User_UserId",
                table: "UrlinfoModel",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UrlinfoModel_User_UserId",
                table: "UrlinfoModel");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "UrlinfoModel",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_UrlinfoModel_User_UserId",
                table: "UrlinfoModel",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
