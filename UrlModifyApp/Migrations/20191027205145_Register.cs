﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UrlModifyApp.Migrations
{
    public partial class Register : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "UrlinfoModel",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    EmailAddress = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UrlinfoModel_UserId",
                table: "UrlinfoModel",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UrlinfoModel_User_UserId",
                table: "UrlinfoModel",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UrlinfoModel_User_UserId",
                table: "UrlinfoModel");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropIndex(
                name: "IX_UrlinfoModel_UserId",
                table: "UrlinfoModel");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "UrlinfoModel");
        }
    }
}
