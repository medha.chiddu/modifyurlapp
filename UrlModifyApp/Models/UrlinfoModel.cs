﻿using System;

namespace UrlModifyApp.Models
{
    public class UrlinfoModel
    {
        public int Id { get; set; }

        public string LongUrl { get; set; }

        public string ShortUrl { get; set; }

        public Guid UserId { get; set; }

        public User User { get; set; }
    }
}