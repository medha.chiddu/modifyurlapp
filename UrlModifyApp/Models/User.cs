﻿using System;
using System.Collections.Generic;

namespace UrlModifyApp.Models
{
    public class User
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public List<UrlinfoModel> UrlinfoModels { get; set; }
    }
}