﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace UrlModifyApp.Services
{
    public class ShortenUrlService : IShortenUrlService
    {
        private readonly string _bitlyToken;
        private static readonly string _baseUrl = "https://api-ssl.bitly.com";

        public ShortenUrlService()
        {

            _bitlyToken = "58254ebdeb2a2196389eac2a8e4f378a60f5f6c0";

            if (string.IsNullOrEmpty(_bitlyToken))
            {
                throw new ArgumentNullException("Bitly Token", "Bitly token cannot be empty");
            }
        }

        public async Task<string> ShortenUrlAsync(string longUrl)
        {
            var url = string.Format("{0}/v3/shorten?access_token={1}&longUrl={2}",
                _baseUrl, _bitlyToken, HttpUtility.UrlEncode(longUrl));

            var request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                var response = await request.GetResponseAsync();
                using (var responseStream = response.GetResponseStream())
                {
                    var reader = new StreamReader(responseStream, Encoding.UTF8);
                    var jsonResponse = JObject.Parse(await reader.ReadToEndAsync());
                    var statusCode = jsonResponse["status_code"].Value<int>();
                    if (statusCode == (int)HttpStatusCode.OK)
                    {
                        return jsonResponse["data"]["url"].Value<string>();
                    }
                    else
                    {
                        return longUrl;
                    }
                }
            }
            catch (WebException ex)
            {
                var errorResponse = ex.Response;
                using (var responseStream = errorResponse.GetResponseStream())
                {
                    var reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    var errorText = reader.ReadToEnd();
                }
            }

            return null;
        }
    }
}