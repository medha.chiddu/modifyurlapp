﻿namespace UrlModifyApp.Services
{
    public interface IPasswordHashers
    {
        string HashPassword(string password);

        bool VerifyHashedPassword(string hashedPassword, string providedPassword);
    }
}