﻿using System.Threading.Tasks;

namespace UrlModifyApp.Services
{
    public interface IShortenUrlService
    {
        Task<string> ShortenUrlAsync(string longUrl);
    }
}