# README #

This application converts long url to short url. User can either sign up and create short url for future data retrieval or they can continue as guest and create short url. Guest user short url will not be saved. 

### About the solution ###

* Solution is built using .net core 2.2 version 
* Database used POSTGRES
* Bitly API is used to convert long URL to short URl

###Working of solution ###

User can either continue as guest or register as user. Logged in user can create short url using long URl. This will be saved to database for future access. Logged in user will be able to see list of url matching their id in their home profile. 
They can edit, delete , view the backed up urls. 
Guest user can create short url and this will not be backed up in database.

## Building the Project

The project is built via `dotnet build`. If run in the root directory of the solution,  the main project will be built.

## Running the Project

You can start the project from the command line via `dotnet run --project UrlModifyApp`.

You must set the `UrlModifyApp_CONNECTION` environment variable in your system environment variables,
 and set it to a value of `Server=127.0.0.1;Port=5432;User Id=<postgresUserName>;Password=<postgresPassword>;Database=<databaseName (defaults to same as username)>;


 You must sign up for bitly (https://app.bitly.com) and get token (https://support.bitly.com/hc/en-us/articles/230647907-How-do-I-find-my-OAuth-access-token-) 
  - You must set `UrlModifyApp_bitlyToken` environment variable with your bitly token 


 ## Running Migrations

To run migrations from the command line, you can use `dotnet ef database update`. Note that you have to specify the `UrlModifyApp_CONNECTION` environment variable the same as above.

### Useful commands to know for dealing with database migrations

- `dotnet ef database update` _(updates the database and applies all existing migrations)_
- `dotnet ef migrations add (Name of the migration)` _(Adds a new migration for any changes to a database context)_
- `dotnet ef database drop` _(drops the database that entity framework core is tracking)_
- `dotnet add package (Nuget package name)` _(Adds a nuget package to the .csproj)_

# Suggested vs code extensions:

- .NET Core Extension Pack [VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=doggy8088.netcore-extension-pack)
- Bracket Pair Colorizer [VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer)
- C# FixFormat [VS Marketplace Link](https://marketplace.visualstudio.com/items?itemName=Leopotam.csharpfixformat)
  - Set the settings mentioned in the documentation for that extension in order to meet Microsoft code formatting standards. This is done in the user settings